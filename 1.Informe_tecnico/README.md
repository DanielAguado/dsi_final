1. Informe Técnico
Esta entrega supone el informe final del trabajo de ahí su importancia y sus contenidos. Se busca dar el cierre a todas las tareas realizadas durante el desarrollo de la asignatura entregando un trabajo completo desde el planteamiento del problema hasta sus conclusiones.

Plantillas

Este informe técnico se debe presentar en un documento que siga el formato de ACM para artículos de congreso con un tamaño límite de 8 páginas en formato doble columna  (Tighter Alternate Style). En caso de necesitar más espacio los autores pueden utilizar apéndices.

ACM SIG Proceedings template

En el siguiente enlace disponéis de un ejemplo de utilización de la plantilla en la que se presentan los puntos que debe tener el informe y un breve comentario sobre su contenido. Se han hecho ciertas modificaciones sobre la plantilla original para facilitar el trabajo sobre ella (fichero ejemplo.tex)
