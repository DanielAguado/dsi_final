2. Código Fuente/Informe de Laboratorio
Libro de Laboratorio

  Los trabajos prácticos se deberán presentar en una única entrega con un documento que debe seguir el formato de "Libro de Laboratorio" (Lab Book). En el se incluirán las descripciones de los experimentos realizados para resolver el problema (datos seleccionados, procesos y algoritmos ejecutados, resultados obtenidos, conclusiones, etc.).

Libros de laboratorio en Latex

La entrega de este "lab book" también se realizará en formato PDF

Ejemplo de Libro de Laboratorio
