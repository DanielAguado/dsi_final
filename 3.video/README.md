3. Video/Presentación
Los puntos que debe contener esta presentación serán:

1. ¿Qué problema se pretende resolver?
2. ¿Cón que datos contáis?
3. ¿Qué pasos habéis seguido para resolverlo?
4. ¿Qué resultados habéis obtenido?
5. ¿Cuáles son los siguientes pasos que daríais?

Todos los puntos son igualmente importantes. tambíen es importante la participación de todos los miembros del grupo.

Se hará accesible mediante enlace un video con una grabación de máximo 15 minutos con el contenido de esta presentación.
