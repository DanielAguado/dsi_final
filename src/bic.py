from time import time
from sklearn import cluster
from scipy.spatial import distance
import sklearn.datasets
from sklearn.preprocessing import StandardScaler
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import sys

def load_data(database):
    f = open(database, "r")
    data = []
    for l in f.readlines():
        row = l.split(",")
        del row[2:4]
        del row[0]
        try:
            data.append(map(float,row))
        except ValueError:
            print row
    return np.asarray(data)

def compute_bic(kmeans,X):
    """
    Computes the BIC metric for a given clusters

    Parameters:
    -----------------------------------------
    kmeans:  List of clustering object from scikit learn
    X     :  multidimension np array of data points

    Returns:
    -----------------------------------------
    BIC value
    """
    # assign centers and labels
    centers = [kmeans.cluster_centers_]
    labels  = kmeans.labels_
    #number of clusters
    m = kmeans.n_clusters
    # size of the clusters
    n = np.bincount(labels)
    #size of data set
    N, d = X.shape

    #compute variance for all clusters beforehand
    cl_var = (1.0 / (N - m) / d) * sum([sum(distance.cdist(X[np.where(labels == i)], [centers[0][i]], 'euclidean')**2) for i in range(m)])

    const_term = 0.5 * m * np.log(N) * (d+1)

    BIC = np.sum([n[i] * np.log(n[i]) -
               n[i] * np.log(N) -
             ((n[i] * d) / 2) * np.log(2*np.pi*cl_var) -
             ((n[i] - 1) * d/ 2) for i in range(m)]) - const_term

    return(BIC)


def bic(data, kmeans_init):

    # IRIS DATA
    tiempo_inicial = time() 
    X = data
    print 'datos cargados'
    ks = np.arange(5,20,1)

    # run 9 times kmeans and save each result in the KMeans object
    KMeans = [cluster.KMeans(n_clusters = i, init=kmeans_init).fit(X) for i in ks]
    print 'kmeans done'
    # now run for each cluster the BIC computation
    BIC = [compute_bic(kmeansi,X) for kmeansi in KMeans]
    print 'bic done'
    my_dpi = 96
    fig = plt.figure(figsize=(2000/my_dpi, 2000/my_dpi), dpi=my_dpi)

    plt.plot(ks,BIC,'r-o')
    plt.title("iris data  (cluster vs BIC)")
    plt.xlabel("# clusters") 
    plt.ylabel("# BIC")

    fig.savefig('graficas/BIC_'+sys.argv[1]+'_'+kmeans_init+'.png')
    plt.close(fig)

    tiempo_final = time() 
    tiempo_ejecucion = tiempo_final - tiempo_inicial
    print 'El tiempo de ejecucion fue:',tiempo_ejecucion

if __name__ == "__main__":
    data  = load_data(sys.argv[1])
    bic(data, "k-means++")
    bic(data, 'random')
