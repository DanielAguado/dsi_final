#!/usr/bin/python3

import sqlite3
import os
import sys
import datetime
import getopt
import math


def print_query(c, query):
    c.execute(query)

    columns = [description[0] for description in c.description]
    print(','.join(columns))

    for r in c.fetchall():
        print ','.join(map(str, r))
    print ('\n\n')


if __name__ == "__main__":
    databasefile = 'database.db'
    print('\n\n')
    conn = sqlite3.connect(databasefile)
    c = conn.cursor()

    print ("Numero de entradas")
    query = "Select count(*) from dataset;"
    print_query(c, query)


    print ("Numero por CNAE")
    query = "Select cnae, count(*) from dataset group by cnae;"
    print_query(c, query)


    print ("Numero por mercado")
    query = "Select mercado, count(*) from dataset group by mercado;"
    print_query(c, query)


    print ("Numero de entradas en Madrid")
    query = "Select count(*) from dataset where de_municip = 'MADRID';"
    print_query(c, query)

    print ("Numero de entradas en Torrox")
    query = "Select count(*) from dataset where de_municip = 'TORROX';"
    print_query(c, query)

    print ("Numero de entradas en Sant Boi de LLobregat")
    query = "Select count(*) from dataset where de_municip = 'SANT BOI DE LLOBREGAT';"
    print_query(c, query)

    print ("Numero sin fecha de alta")
    query = "Select count(*) from dataset where fecha_alta_stro = 0;"
    print_query(c, query)

    print ("Numero por CNAE en Madrid")
    query = "Select cnae, count(*) from dataset where de_municip = 'MADRID' group by cnae;"
    print_query(c, query)

    print ("Numero por mercado en Madrid")
    query = "Select mercado, count(*) from dataset where de_municip = 'MADRID' group by mercado;"
    print_query(c, query)

    print ("Numero sin fecha de alta en Madrid")
    query = "Select count(*) from dataset where fecha_alta_stro= 0 and de_municip = 'MADRID';"
    print_query(c, query)

    print ("Numero por CNAE en Torrox")
    query = "Select cnae, count(*) from dataset where de_municip = 'TORROX' group by cnae;"
    print_query(c, query)

    print ("Numero por mercado en Torrox")
    query = "Select mercado, count(*) from dataset where de_municip = 'TORROX' group by mercado;"
    print_query(c, query)

    print ("Numero sin fecha de alta en Torrox")
    query = "Select count(*) from dataset where fecha_alta_stro= 0 and de_municip = 'TORROX';"
    print_query(c, query)

    print ("Productos en mercado regulado")
    query = "Select producto, count(*) from dataset where mercado = 'M1' group by producto;"
    print_query(c, query)

    print ("Número de clientes en Madrid")
    query = "select count(*) from (select identificador, count(*) from dataset where de_municip = 'MADRID' group by Identificador);"
    print_query(c, query)

    print ("Número de clientes en Torrox")
    query = "select count(*) from (select identificador, count(*) from dataset where de_municip = 'TORROX' group by Identificador);"
    print_query(c, query) 



    print ("Variables de consumo por producto")
    query = "select producto, \
        sum(activa_total) as agregado_activa_total, sum(reactiva_total) as agregado_reactiva_total, \
        max(activa_total) as max_activa_total, max(reactiva_total) as max_reactiva_total, \
        min(activa_total) as min_activa_total, min(reactiva_total) as min_reactiva_total, \
        avg(activa_total) as avg_activa_total, avg(reactiva_total) as avg_reactiva_total \
        from \
        (select producto, (activa_h1 + activa_h2 + activa_h3 + activa_h4 + activa_h5 + activa_h6 \
        + activa_h7 + activa_h8 + activa_h9 + activa_h10 + activa_h11 \
        + activa_h12 + activa_h13 + activa_h14 + activa_h15 + activa_h16 \
        + activa_h17 + activa_h18 + activa_h19 + activa_h20 + activa_h21 \
        + activa_h22 + activa_h23 + activa_h24 + activa_h25) as activa_total, \
        (reactiva_h1 + reactiva_h2 + reactiva_h3 + reactiva_h4 + reactiva_h5 \
        + reactiva_h6 + reactiva_h7 + reactiva_h8 + reactiva_h9 + reactiva_h10 \
        + reactiva_h11 + reactiva_h12 + reactiva_h13 + reactiva_h14 + reactiva_h15 \
        + reactiva_h16 + reactiva_h17 + reactiva_h18 + reactiva_h19 + reactiva_h20 \
        + reactiva_h21 + reactiva_h22 + reactiva_h23 + reactiva_h24 + reactiva_h25) as reactiva_total \
        from dataset) group by producto"
    print_query(c, query)

    print ("Media, maxima y minima por hora catalogado por productos")
    query = 'Select producto'
    for x in xrange(1,26):
        query = query + ",avg(Activa_h{0}),max(Activa_h{0}),min(Activa_h{0})".format(x)
    query = query + ' from dataset group by PRODUCTO'
    print_query(c,query)

