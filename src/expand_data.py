#!/usr/bin/python3

import sqlite3
import os
import sys
import datetime
import getopt
import math

def categorize(conn):
    c = conn.cursor()

    query = "CREATE TABLE dia (ID INTEGER PRIMARY KEY AUTOINCREMENT, dia);"
    c.execute(query)
    conn.commit()

    query = "insert into dia (dia) select dia from dataset group by dia;"
    c.execute(query)
    conn.commit()

    query = "CREATE TABLE fecha_alta_stro (ID INTEGER PRIMARY KEY AUTOINCREMENT, fecha_alta_stro);"
    c.execute(query)
    conn.commit()

    query = "insert into fecha_alta_stro (fecha_alta_stro) select fecha_alta_stro from dataset where fecha_alta_stro != 0 group by fecha_alta_stro;"
    c.execute(query)
    conn.commit()

    query = "CREATE TABLE cnae (ID INTEGER PRIMARY KEY AUTOINCREMENT, cnae);"
    c.execute(query)
    conn.commit()

    query = "insert into cnae (cnae) select cnae from dataset group by cnae;"
    c.execute(query)
    conn.commit()

    query = "CREATE TABLE producto (ID INTEGER PRIMARY KEY AUTOINCREMENT, producto);"
    c.execute(query)
    conn.commit()

    query = "insert into producto (producto) select producto from dataset group by producto;"
    c.execute(query)
    conn.commit()

    query = "CREATE TABLE mercado (ID INTEGER PRIMARY KEY AUTOINCREMENT, mercado);"
    c.execute(query)
    conn.commit()

    query = "insert into mercado (mercado) select mercado from dataset group by mercado;"
    c.execute(query)
    conn.commit()

    query = "CREATE TABLE de_municip (ID INTEGER PRIMARY KEY AUTOINCREMENT, de_municip);"
    c.execute(query)
    conn.commit()

    query = "insert into de_municip (de_municip) select de_municip from dataset group by de_municip;"
    c.execute(query)
    conn.commit()

def expand_data_madrid(conn):
    c = conn.cursor()
    query = "create table madrid as select b.id as dia_id, a.id as fecha_alta_stro_id, \
            identificador, c.id as cnae_id, p.id as producto_id, m.id as mercado_id, \
            activa_h1, activa_h2, activa_h3, \
            activa_h4, activa_h5, activa_h6, activa_h7, activa_h8, activa_h9, activa_h10, \
            activa_h11, activa_h12,  activa_h13, activa_h14, activa_h15, activa_h16, \
            activa_h17, activa_h18, activa_h19, activa_h20, activa_h21, activa_h22, \
            activa_h23, activa_h24, activa_h25, reactiva_h1, reactiva_h2, reactiva_h3, \
            reactiva_h4, reactiva_h5, reactiva_h6, reactiva_h7, reactiva_h8, reactiva_h9, \
            reactiva_h10, reactiva_h11, reactiva_h12, reactiva_h13, reactiva_h14, \
            reactiva_h15, reactiva_h16, reactiva_h17, reactiva_h18, reactiva_h19, \
            reactiva_h20, reactiva_h21, reactiva_h22, reactiva_h23, reactiva_h24, \
            reactiva_h25, (activa_h1 + activa_h2 + activa_h3 + activa_h4 + activa_h5 + activa_h6 \
            + activa_h7 + activa_h8 + activa_h9 + activa_h10 + activa_h11 \
            + activa_h12 + activa_h13 + activa_h14 + activa_h15 + activa_h16 \
            + activa_h17 + activa_h18 + activa_h19 + activa_h20 + activa_h21 \
            + activa_h22 + activa_h23 + activa_h24 + activa_h25) as activa_total, \
            (reactiva_h1 + reactiva_h2 + reactiva_h3 + reactiva_h4 + reactiva_h5 \
            + reactiva_h6 + reactiva_h7 + reactiva_h8 + reactiva_h9 + reactiva_h10 \
            + reactiva_h11 + reactiva_h12 + reactiva_h13 + reactiva_h14 + reactiva_h15 \
            + reactiva_h16 + reactiva_h17 + reactiva_h18 + reactiva_h19 + reactiva_h20 \
            + reactiva_h21 + reactiva_h22 + reactiva_h23 + reactiva_h24 + reactiva_h25) \
            as reactiva_total \
            from dataset d \
            inner join fecha_alta_stro a on d.fecha_alta_stro = a.fecha_alta_stro \
            inner join dia b on d.dia = b.dia \
            inner join cnae c on d.cnae = c.cnae \
            inner join producto p on d.producto = p.producto \
            inner join mercado m on d.mercado = m.mercado \
            where d.fecha_alta_stro != 0 \
            and de_municip = 'MADRID';"
    c.execute(query)
    conn.commit()

def expand_data_torrox(conn):
    c = conn.cursor()
    query = "create table torrox as select b.id as dia_id, a.id as fecha_alta_stro_id, \
            identificador, c.id as cnae_id, p.id as producto_id, m.id as mercado_id, \
            activa_h1, activa_h2, activa_h3, \
            activa_h4, activa_h5, activa_h6, activa_h7, activa_h8, activa_h9, activa_h10, \
            activa_h11, activa_h12,  activa_h13, activa_h14, activa_h15, activa_h16, \
            activa_h17, activa_h18, activa_h19, activa_h20, activa_h21, activa_h22, \
            activa_h23, activa_h24, activa_h25, reactiva_h1, reactiva_h2, reactiva_h3, \
            reactiva_h4, reactiva_h5, reactiva_h6, reactiva_h7, reactiva_h8, reactiva_h9, \
            reactiva_h10, reactiva_h11, reactiva_h12, reactiva_h13, reactiva_h14, \
            reactiva_h15, reactiva_h16, reactiva_h17, reactiva_h18, reactiva_h19, \
            reactiva_h20, reactiva_h21, reactiva_h22, reactiva_h23, reactiva_h24, \
            reactiva_h25, (activa_h1 + activa_h2 + activa_h3 + activa_h4 + activa_h5 + activa_h6 \
            + activa_h7 + activa_h8 + activa_h9 + activa_h10 + activa_h11 \
            + activa_h12 + activa_h13 + activa_h14 + activa_h15 + activa_h16 \
            + activa_h17 + activa_h18 + activa_h19 + activa_h20 + activa_h21 \
            + activa_h22 + activa_h23 + activa_h24 + activa_h25) as activa_total, \
            (reactiva_h1 + reactiva_h2 + reactiva_h3 + reactiva_h4 + reactiva_h5 \
            + reactiva_h6 + reactiva_h7 + reactiva_h8 + reactiva_h9 + reactiva_h10 \
            + reactiva_h11 + reactiva_h12 + reactiva_h13 + reactiva_h14 + reactiva_h15 \
            + reactiva_h16 + reactiva_h17 + reactiva_h18 + reactiva_h19 + reactiva_h20 \
            + reactiva_h21 + reactiva_h22 + reactiva_h23 + reactiva_h24 + reactiva_h25) \
            as reactiva_total \
            from dataset d \
            inner join fecha_alta_stro a on d.fecha_alta_stro = a.fecha_alta_stro \
            inner join dia b on d.dia = b.dia \
            inner join cnae c on d.cnae = c.cnae \
            inner join producto p on d.producto = p.producto \
            inner join mercado m on d.mercado = m.mercado \
            where d.fecha_alta_stro != 0 \
            and de_municip = 'TORROX';"
    c.execute(query)
    conn.commit()

def expand_data_all(conn):
    c = conn.cursor()
    query = "create table dataset_expanded as select b.id as dia_id, a.id as fecha_alta_stro_id, \
            identificador, c.id as cnae_id, p.id as producto_id, m.id as mercado_id, \
            activa_h1, activa_h2, activa_h3, \
            activa_h4, activa_h5, activa_h6, activa_h7, activa_h8, activa_h9, activa_h10, \
            activa_h11, activa_h12,  activa_h13, activa_h14, activa_h15, activa_h16, \
            activa_h17, activa_h18, activa_h19, activa_h20, activa_h21, activa_h22, \
            activa_h23, activa_h24, activa_h25, reactiva_h1, reactiva_h2, reactiva_h3, \
            reactiva_h4, reactiva_h5, reactiva_h6, reactiva_h7, reactiva_h8, reactiva_h9, \
            reactiva_h10, reactiva_h11, reactiva_h12, reactiva_h13, reactiva_h14, \
            reactiva_h15, reactiva_h16, reactiva_h17, reactiva_h18, reactiva_h19, \
            reactiva_h20, reactiva_h21, reactiva_h22, reactiva_h23, reactiva_h24, \
            reactiva_h25, (activa_h1 + activa_h2 + activa_h3 + activa_h4 + activa_h5 + activa_h6 \
            + activa_h7 + activa_h8 + activa_h9 + activa_h10 + activa_h11 \
            + activa_h12 + activa_h13 + activa_h14 + activa_h15 + activa_h16 \
            + activa_h17 + activa_h18 + activa_h19 + activa_h20 + activa_h21 \
            + activa_h22 + activa_h23 + activa_h24 + activa_h25) as activa_total, \
            (reactiva_h1 + reactiva_h2 + reactiva_h3 + reactiva_h4 + reactiva_h5 \
            + reactiva_h6 + reactiva_h7 + reactiva_h8 + reactiva_h9 + reactiva_h10 \
            + reactiva_h11 + reactiva_h12 + reactiva_h13 + reactiva_h14 + reactiva_h15 \
            + reactiva_h16 + reactiva_h17 + reactiva_h18 + reactiva_h19 + reactiva_h20 \
            + reactiva_h21 + reactiva_h22 + reactiva_h23 + reactiva_h24 + reactiva_h25) \
            as reactiva_total \
            from dataset d \
            inner join fecha_alta_stro a on d.fecha_alta_stro = a.fecha_alta_stro \
            inner join dia b on d.dia = b.dia \
            inner join cnae c on d.cnae = c.cnae \
            inner join producto p on d.producto = p.producto \
            inner join mercado m on d.mercado = m.mercado \
            where d.fecha_alta_stro != 0;"
    c.execute(query)
    conn.commit()

def expand_data_clients(conn):
    c = conn.cursor()

    query = 'create table clients as select \
             identificador, cnae.id, producto.id, mercado.id, de_municip.id'
    for x in xrange(1,26):
        query = query + ", avg(Activa_h{0}) as avg_Activa_h{0}, \
                         max(Activa_h{0}) as max_activa_h{0}, \
                         min(Activa_h{0}) as min_activa_h_{0}, \
                         avg(reactiva_h{0}) as avg_reactiva_h{0}, \
                         max(reactiva_h{0}) as max_reactiva_h{0}, \
                         min(reactiva_h{0}) as min_reactiva_h_{0} \
                            ".format(x)
    query = query + ' from dataset d \
            inner join fecha_alta_stro a on d.fecha_alta_stro = a.fecha_alta_stro \
            inner join cnae cnae on d.cnae = cnae.cnae \
            inner join producto producto on d.producto = producto.producto \
            inner join mercado mercado on d.mercado = mercado.mercado \
            inner join de_municip de_municip on d.de_municip = de_municip.de_municip \
            where d.fecha_alta_stro != 0 \
            group by identificador;'
    c.execute(query)
    conn.commit()

def export_to_csv(conn, table):
    filename = '%s.csv' % table
    print ("export file %s" % filename)

    c = conn.cursor()

    query = 'select * from %s' % (table)
    result = c.execute(query)

    columns = [description[0] for description in c.description]

    with open(filename, 'w') as f:        
        f.write('%s\n' % (','.join(columns)))
        for r in result:
            line = ""
            for d in r:
                line = "%s,%s" % (line, d)
            f.write("%s\n" % line[1:])

if __name__ == "__main__":
    databasefile = 'database.db'
    
    conn = sqlite3.connect(databasefile)
    print 'connect to db'

    categorize(conn)

    expand_data_madrid(conn)
    expand_data_torrox(conn)
    expand_data_all(conn)
    expand_data_clients(conn)

    export_to_csv(conn, 'madrid')
    export_to_csv(conn, 'torrox')
    export_to_csv(conn, 'dataset_expanded')
    export_to_csv(conn, 'clients')
