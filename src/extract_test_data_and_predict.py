#!/usr/bin/python3

import sqlite3
import os
import sys
import datetime
import random
import math


def get_test_data(c, id, number):

    data = []
    query = "select * from dataset where identificador = %s limit 100" % (id)
    result = c.execute(query).fetchall()

    results_len = len(result)
    for i in range(0, number):
        data.append(result[random.randint(0, results_len - 1)])

    return data

def get_dataset_clusterized(filename):
    dataset = dict()
    first_line = True
    with open(filename, 'r') as f:
        for line in f:
            data = line.replace('\n', '').split(',')
            if first_line == True:
                first_line = False
            else:
                key = data[-2]

                if key not in dataset:
                    dataset[key] = []

                dataset[key].append([float(d) for d in data])

    return dataset

def mean(numbers):
    return sum(numbers)/float(len(numbers))

def stdev(numbers):
    avg = mean(numbers)
    variance = sum([pow(x-avg,2) for x in numbers])/float(len(numbers)-1)
    return math.sqrt(variance)

def summarize(dataset):
    summaries = [(mean(attribute), stdev(attribute)) for attribute in zip(*dataset)]
    del summaries[-1]

    return summaries

def summarizeByClass(data):
    summaries = dict()
    for classValue, instances in data.items():
        summaries[classValue] = summarize(instances)
    return summaries

def calculateProbability(x, mean, stdev):
    try:
        exponent = math.exp(-(math.pow(x-mean,2)/(2*math.pow(stdev,2))))
        return (1 / (math.sqrt(2*math.pi) * stdev)) * exponent
    except:
        print ("x: %s\tmean: %s\tstdev: %s" % (x, mean, stdev))
        return 0

def calculateClassProbabilities(summaries, inputVector):
    probabilities = dict()
    for classValue, classSummaries in summaries.items():
        probabilities[classValue] = 1
        for i in range(len(classSummaries)):
            mean, stdev = classSummaries[i]
            x = inputVector[i]
            probabilities[classValue] *= calculateProbability(x, mean, stdev)
    return probabilities

def get_membership_percent(probabilities):
    print ("prob: %s" % probabilities)
    membership_percent = dict()
    aux = []

    for classValue, probability in probabilities.items():
        aux.append((classValue, probability))

    for i in range(len(aux)):
        membership_percent[aux[i][0]] =  100 * aux[i][1] / (sum([x[1]  for x in aux]))

    return membership_percent


def predict(summaries, inputVector):
    probabilities = calculateClassProbabilities(summaries, inputVector)
    bestLabel, bestProb = None, -1
    for classValue, probability in probabilities.items():
        if bestLabel is None or probability > bestProb:
            bestProb = probability
            bestLabel = classValue

    return bestLabel, get_membership_percent(probabilities)

def getPredictions(summaries, testSet):
    predictions = []
    membership_percent_group = []
    for i in range(len(testSet)):
        result, membership_percent = predict(summaries, testSet[i])
        predictions.append(result)
        membership_percent_group.append(membership_percent)
    return predictions, membership_percent_group
 
def getAccuracy(testSet, predictions):
    correct = 0
    for x in range(len(testSet)):
        if testSet[x][-1] == predictions[x]:
            correct += 1
    return (correct/float(len(testSet))) * 100.0


if __name__ == '__main__':
    filename = sys.argv[1]
    number = sys.argv[2]

    dataset_clusterized = get_dataset_clusterized(filename)

    '''
    databasefile = 'database.db'

    print 'connect to db'    
    conn = sqlite3.connect(databasefile)
    c = conn.cursor()

    test_data = []
    test_clasified_on = []
    for key, value in dataset_clusterized.items():
        id = value[0][-1]
        test_data.append(get_test_data(c, id, int(number)))
        test_clasified_on.append(key)
    '''
    test_data = [[1,2,1,1,593,89.8297872340426,152,0,40.4964539007092,68,0,88.9645390070922,149,0,39.645390070922,67,0,89.1347517730496,151,0,39.6524822695035,66,0,88.5248226950355,150,0,39.0354609929078,63,0,87.822695035461,151,0,38.1418439716312,62,0,88.2836879432624,149,0,38.290780141844,80,0,89.3120567375886,190,0,37.0070921985816,80,0,141.397163120567,512,0,50.5602836879433,259,0,160.602836879433,580,0,50.6028368794326,302,0,137.460992907801,537,0,48.8368794326241,325,0,139.482269503546,514,0,51.6595744680851,320,0,149.822695035461,863,0,52.5957446808511,320,0,142.354609929078,773,0,52.4397163120567,338,0,144.77304964539,937,0,51.6950354609929,304,0,123.297872340426,500,0,42.4397163120567,253,0,116.723404255319,464,0,37.6595744680851,183,0,116.808510638298,502,0,36.1560283687943,80,0,120.702127659574,413,0,38.5106382978723,206,0,116.971631205674,339,0,37.3404255319149,146,0,114.900709219858,342,0,37.5886524822695,72,0,119.007092198582,445,0,40.9219858156028,176,0,108.184397163121,332,0,42.9503546099291,144,0,94.8439716312057,342,0,42.241134751773,145,0,91.1489361702128,152,0,41.2198581560284,66,0,0.0,0,0,0.0,0,0]]

    summaries = summarizeByClass(dataset_clusterized)
    #print (summaries)
    predictions, membership_percent_group = getPredictions(summaries, test_data)
    accuracy = getAccuracy(test_data, predictions)
    #print (accuracy)
    print('Accuracy: {0}%').format(accuracy)
    print('membership percents: {0}%').format(membership_percent_group)