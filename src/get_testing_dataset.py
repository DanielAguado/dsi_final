#!/usr/bin/python3

import sqlite3
import os
import sys
import datetime
import random
import math

def get_dataset_clusterized(filename):
    dataset = []
    first_line = True

    with open(filename, 'r') as f:
        for line in f:
            data = line.replace('\n', '').split(',')
            if first_line == True:
                first_line = False
            else:
                dataset.append((data[-1], data[-2]))

    return dataset

def get_test_data(c, id, number, category):

    data = []
    query = "select cnae.id, de_municip.id, \
        activa_h1, reactiva_h1, activa_h2, reactiva_h2, activa_h3, reactiva_h3, activa_h4, \
        reactiva_h4, activa_h5, reactiva_h5, activa_h6, reactiva_h6, activa_h7, reactiva_h7, \
        activa_h8, reactiva_h8, activa_h9, reactiva_h9, activa_h10, reactiva_h10, activa_h11, \
        reactiva_h11, activa_h12, reactiva_h12, activa_h13, reactiva_h13, activa_h14, \
        reactiva_h14, activa_h15, reactiva_h15, activa_h16, reactiva_h16, activa_h17, \
        reactiva_h17, activa_h18, reactiva_h18, activa_h19, reactiva_h19, activa_h20, \
        reactiva_h20, activa_h21, reactiva_h21, activa_h22, reactiva_h22, activa_h23, \
        reactiva_h23, activa_h24, reactiva_h24, activa_h25, reactiva_h25, \
        identificador from dataset d \
        inner join cnae cnae on d.cnae = cnae.cnae \
        inner join producto producto on d.producto = producto.producto \
        inner join mercado mercado on d.mercado = mercado.mercado \
        inner join de_municip de_municip on d.de_municip = de_municip.de_municip \
        where identificador = %s limit 100;" % (id)

    result = c.execute(query).fetchall()

    results_len = len(result)
    tmp = []
    for i in range(0, number):
        tmp.append(result[random.randint(0, results_len - 1)])

    maximum = dict()
    minimum = dict()
    average = dict()

    for j in range(2,53):
        maximum[j] = 0
        minimum[j] = 1000000
        average[j] = 0

    for i in tmp:
        for j in range (2, 53):
            if i[j] > maximum[j]:
                maximum[j] = i[j]
            if i[j] < minimum[j]:
                minimum[j] = i[j]
            average[j] = i[j] / len(tmp)

    data = [tmp[1][0], tmp[1][1]]
    for i in range(2, 52):
        data = data + [average[i], maximum[i], minimum[i]]
    data = data + [int(category), tmp[1][-1]]

    return data

def export_to_csv(dataset, filename):
    print ("export file %s" % filename)
    columns = ['cnae.id', 'de_municip.id', 'avg_Activa_h1', 'max_activa_h1', 'min_activa_h_1', 'avg_reactiva_h1', 'max_reactiva_h1', 'min_reactiva_h_1', 'avg_Activa_h2', 'max_activa_h2', 'min_activa_h_2', 'avg_reactiva_h2', 'max_reactiva_h2', 'min_reactiva_h_2', 'avg_Activa_h3', 'max_activa_h3', 'min_activa_h_3', 'avg_reactiva_h3', 'max_reactiva_h3', 'min_reactiva_h_3', 'avg_Activa_h4', 'max_activa_h4', 'min_activa_h_4', 'avg_reactiva_h4', 'max_reactiva_h4', 'min_reactiva_h_4', 'avg_Activa_h5', 'max_activa_h5', 'min_activa_h_5', 'avg_reactiva_h5', 'max_reactiva_h5', 'min_reactiva_h_5', 'avg_Activa_h6', 'max_activa_h6', 'min_activa_h_6', 'avg_reactiva_h6', 'max_reactiva_h6', 'min_reactiva_h_6', 'avg_Activa_h7', 'max_activa_h7', 'min_activa_h_7', 'avg_reactiva_h7', 'max_reactiva_h7', 'min_reactiva_h_7', 'avg_Activa_h8', 'max_activa_h8', 'min_activa_h_8', 'avg_reactiva_h8', 'max_reactiva_h8', 'min_reactiva_h_8', 'avg_Activa_h9', 'max_activa_h9', 'min_activa_h_9', 'avg_reactiva_h9', 'max_reactiva_h9', 'min_reactiva_h_9', 'avg_Activa_h10', 'max_activa_h10', 'min_activa_h_10', 'avg_reactiva_h10', 'max_reactiva_h10', 'min_reactiva_h_10', 'avg_Activa_h11', 'max_activa_h11', 'min_activa_h_11', 'avg_reactiva_h11', 'max_reactiva_h11', 'min_reactiva_h_11', 'avg_Activa_h12', 'max_activa_h12', 'min_activa_h_12', 'avg_reactiva_h12', 'max_reactiva_h12', 'min_reactiva_h_12', 'avg_Activa_h13', 'max_activa_h13', 'min_activa_h_13', 'avg_reactiva_h13', 'max_reactiva_h13', 'min_reactiva_h_13', 'avg_Activa_h14', 'max_activa_h14', 'min_activa_h_14', 'avg_reactiva_h14', 'max_reactiva_h14', 'min_reactiva_h_14', 'avg_Activa_h15', 'max_activa_h15', 'min_activa_h_15', 'avg_reactiva_h15', 'max_reactiva_h15', 'min_reactiva_h_15', 'avg_Activa_h16', 'max_activa_h16', 'min_activa_h_16', 'avg_reactiva_h16', 'max_reactiva_h16', 'min_reactiva_h_16', 'avg_Activa_h17', 'max_activa_h17', 'min_activa_h_17', 'avg_reactiva_h17', 'max_reactiva_h17', 'min_reactiva_h_17', 'avg_Activa_h18', 'max_activa_h18', 'min_activa_h_18', 'avg_reactiva_h18', 'max_reactiva_h18', 'min_reactiva_h_18', 'avg_Activa_h19', 'max_activa_h19', 'min_activa_h_19', 'avg_reactiva_h19', 'max_reactiva_h19', 'min_reactiva_h_19', 'avg_Activa_h20', 'max_activa_h20', 'min_activa_h_20', 'avg_reactiva_h20', 'max_reactiva_h20', 'min_reactiva_h_20', 'avg_Activa_h21', 'max_activa_h21', 'min_activa_h_21', 'avg_reactiva_h21', 'max_reactiva_h21', 'min_reactiva_h_21', 'avg_Activa_h22', 'max_activa_h22', 'min_activa_h_22', 'avg_reactiva_h22', 'max_reactiva_h22', 'min_reactiva_h_22', 'avg_Activa_h23', 'max_activa_h23', 'min_activa_h_23', 'avg_reactiva_h23', 'max_reactiva_h23', 'min_reactiva_h_23', 'avg_Activa_h24', 'max_activa_h24', 'min_activa_h_24', 'avg_reactiva_h24', 'max_reactiva_h24', 'min_reactiva_h_24', 'avg_Activa_h25', 'max_activa_h25', 'min_activa_h_25', 'avg_reactiva_h25', 'max_reactiva_h25', 'min_reactiva_h_25', 'category', 'indentificador']

    with open(filename, 'w') as f:
        f.write('%s\n' % (','.join(columns)))
        for d in dataset:
            f.write("%s\n" % (','.join([str(v) for v in d])))

if __name__ == '__main__':
    clustered_dataset = sys.argv[1]

    databasefile = 'database.db'
    number_of_clientes = 100000
    entries_per_client = 5

    print ('connect to db')    
    conn = sqlite3.connect(databasefile)
    c = conn.cursor()

    dataset = get_dataset_clusterized(clustered_dataset)
    
    test_data = []
    #testing_dataset_len = number_of_clientes / 10
    testing_dataset_len = 10
    for i in range(testing_dataset_len):
        client_id = random.randint(1, number_of_clientes - 1)
        test_data.append(get_test_data(c, dataset[client_id][0], entries_per_client, dataset[client_id][1]))
        if (i % (testing_dataset_len / 10) == 0):
            print ("Searching data client: {0} of {1}".format(i, testing_dataset_len))

    export_to_csv(test_data, 'testing_dataset.csv')


