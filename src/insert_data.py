#!/usr/bin/python3

import sqlite3
import os
import sys
import datetime
import getopt
import math


def create_table(conn):

    query = "create table dataset (id integer, DIA integer, H1 integer, ACTIVA_H1 integer, REACTIVA_H1 integer, H2 integer, ACTIVA_H2 integer, REACTIVA_H2 integer, H3 integer, ACTIVA_H3 integer, REACTIVA_H3 integer, H4 integer, ACTIVA_H4 integer, REACTIVA_H4 integer, H5 integer, ACTIVA_H5 integer, REACTIVA_H5 integer, H6 integer, ACTIVA_H6 integer, REACTIVA_H6 integer, H7 integer, ACTIVA_H7 integer, REACTIVA_H7 integer, H8 integer, ACTIVA_H8 integer, REACTIVA_H8 integer, H9 integer, ACTIVA_H9 integer, REACTIVA_H9 integer, H10 integer, ACTIVA_H10 integer, REACTIVA_H10 integer, H11 integer, ACTIVA_H11 integer, REACTIVA_H11 integer, H12 integer, ACTIVA_H12 integer, REACTIVA_H12 integer, H13 integer, ACTIVA_H13 integer, REACTIVA_H13 integer, H14 integer, ACTIVA_H14 integer, REACTIVA_H14 integer, H15 integer, ACTIVA_H15 integer, REACTIVA_H15 integer, H16 integer, ACTIVA_H16 integer, REACTIVA_H16 integer, H17 integer, ACTIVA_H17 integer, REACTIVA_H17 integer, H18 integer, ACTIVA_H18 integer, REACTIVA_H18 integer, H19 integer, ACTIVA_H19 integer, REACTIVA_H19 integer, H20 integer, ACTIVA_H20 integer, REACTIVA_H20 integer, H21 integer, ACTIVA_H21 integer, REACTIVA_H21 integer, H22 integer, ACTIVA_H22 integer, REACTIVA_H22 integer, H23 integer, ACTIVA_H23 integer, REACTIVA_H23 integer, H24 integer, ACTIVA_H24 integer, REACTIVA_H24 integer, H25 integer, ACTIVA_H25 integer, REACTIVA_H25 integer, DE_MUNICIP text, FECHA_ALTA_STRO text, TARGET_TENENCIA_CUPS real, IDENTIFICADOR integer, CNAE text, PRODUCTO text, MERCADO text);"


    c = conn.cursor()
    c.execute(query)
    conn.commit()

def to_string(index, line):

    line = line.replace('\n', '').replace("'", "")
    data = "%s" % (index)

    text_positions = [76, 77, 80, 81, 82]
    for i, d in enumerate(line.split('|')):
        if (d == ''):
            d = 0
        if (i in text_positions):
            data = "%s, '%s'" % (data, d)
        else:
            data = "%s, %s" % (data, d)

    return data

def insert_on_db(conn, index, line):
    
    query = "insert into dataset values (%s);" % (to_string(index, line))

    c = conn.cursor()
    try:
        c.execute(query)
    except sqlite3.OperationalError, msg:
        print ("Error on query: %s\n\n%s" % (query, msg))


if __name__ == "__main__":

    inputfile = '../DATATHON_secuencial.csv'
    databasefile = 'database.db'
    
    conn = sqlite3.connect(databasefile)
    print 'connect to db'

    create_table(conn)

    with open(inputfile) as f:
        index = 0
        for line in f:
            insert_on_db(conn, index, line)
            index = index + 1
            if (index % 20000 == 0):
                conn.commit()
                print ('commit')
#                sys.exit()


