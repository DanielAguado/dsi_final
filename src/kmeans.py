#http://scikit-learn.org/stable/auto_examples/cluster/plot_kmeans_digits.html
print(__doc__)

import sys
from time import time
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

from sklearn import metrics
from sklearn.cluster import KMeans
from sklearn.datasets import load_digits
from sklearn.decomposition import PCA
from sklearn import preprocessing
import matplotlib.cm as cmx
import matplotlib.colors as colors

def get_cmap(N):
    '''Returns a function that maps each index in 0, 1, ... N-1 to a distinct 
    RGB color.'''
    color_norm  = colors.Normalize(vmin=0, vmax=N-1)
    scalar_map = cmx.ScalarMappable(norm=color_norm, cmap='hsv') 
    def map_index_to_rgb_color(index):
        return scalar_map.to_rgba(index)
    return map_index_to_rgb_color

def normalize(d):
    min_max_scaler = preprocessing.MinMaxScaler()
    datanorm = min_max_scaler.fit_transform(d)
    return datanorm

def load_data(database):
    f = open(database, "r")
    data = []
    producto = []
    mercado = []
    leyend = ''
    for l in f.readlines():
        row = l.split(",")
        try:
            producto.append(float(row[2]))
            mercado.append(float(row[3]))
            del row[2:4]
            del row[0]
            data.append(map(float,row))
        except ValueError:
            del row[2:4]
            del row[0]
            leyend = row

    return np.asarray(data), np.asarray(producto), np.asarray(mercado), leyend

def bench_k_means(estimator, name, data):
    t0 = time()
    estimator.fit(data)
    inertia = estimator.inertia_
    sample_size = 1000
    silhouette=metrics.silhouette_score(data, estimator.labels_,metric='euclidean',sample_size=sample_size)
    print('{0}\t\t{1}\t{2}\t{3}'.format(name, (time() - t0), inertia,silhouette))
    return silhouette, ;

def do_bench_k_means(data):

    clusters = int(sys.argv[2])
    np.random.seed(42)
    datanorm = normalize(data)
    n_samples, n_features = data.shape
    print("entradas: {1}, \t variables: {2}".format(n_samples, n_features))
    print(79 * '_')
    print('init\t\ttime\tinertia\tsilhouette')
    silhouette_kmeans = bench_k_means(KMeans(init='k-means++', n_clusters=clusters, n_init=10),name="k-means++", data=datanorm)
    silhouette_random = bench_k_means(KMeans(init='random', n_clusters=clusters, n_init=10),name="random", data=datanorm)
    kmeans_init = 'k-means++'
    print(79 * '_')
    return datanorm, kmeans_init

def stats(data,labels, producto, mercado):
    n_clusters = len(np.unique(labels))
    for c in range(n_clusters):
        index = np.where(labels==c)
        clients = data[index]
        productos = producto[index]
        mercados = mercado[index]
        print ('Grupo:\t\t{0}'.format(c))
        print ('Clientes:\t{0}'.format(len(clients)))
        print ('T1:\t\t{0}\t{1}%'.format(len(np.where(clients[:,0]==1.)[0]), (len(np.where(clients[:,0]==1.)[0])*100)/len(clients)))
        print ('T2:\t\t{0}\t{1}%'.format(len(np.where(clients[:,0]==2.)[0]), (len(np.where(clients[:,0]==2.)[0])*100)/len(clients)))
        print ('M1:\t\t{0}\t{1}%'.format(len(np.where(mercados==1.)[0]), (len(np.where(mercados==1.)[0])*100)/len(mercados)))
        print ('M2:\t\t{0}\t{1}%'.format(len(np.where(mercados==2.)[0]), (len(np.where(mercados==2.)[0])*100)/len(mercados)))
        print ('Productos:')
        productoID = np.unique(productos)
        otros = 0
        for i in range(len(productoID)):
            ID = productoID[i]
            productosOfID = np.where(productos==ID)[0]
            productosTotal = len(productos)
            porcentaje = ((len(productosOfID)*100)/productosTotal)
            if(porcentaje==0):
                otros = otros + len(productosOfID)
            else:
                print('ID:{0}\t\t{1}\t{2}%'.format(int(ID), len(productosOfID), porcentaje))
        print('Otros Productos:\t\t{0}\t{1}%'.format(otros, (otros*100)/productosTotal))
        print ('Activa:')
        print('Hora:\tMedia\t\tMax\t\tMin')
        for i in range(25):
            print('{0}\t{1}\t{2}\t\t{3}'.format(i, np.mean(clients[:,(i*6)+2]), max(clients[:,((i*6)+3)]), min(clients[:,((i*6)+4)])))

        print ('Reactiva:')    
        print ('Hora:\tMedia\t\tMax\t\tMin')        
        for i in range(25):
            print('{0}\t{1}\t{2}\t\t{3}'.format(i, np.mean(clients[:,(i*6)+5]), max(clients[:,((i*6)+6)]), min(clients[:,((i*6)+7)])))

def showStatsGraphic(data,labels, producto, mercado):
    n_clusters = len(np.unique(labels))
    size = len(data[0])
    for c in range (0, n_clusters):
        plt.close('all')
        my_dpi = 96
        fig = plt.figure(figsize=(1080/my_dpi, 1920/my_dpi), dpi=my_dpi)
        index = np.where(labels==c)
        clients = data[index]
        productos = producto[index]
        mercados = mercado[index]

        ConsumoAvgActivoTotal = []
        ConsumoMaxActivoTotal = []
        ConsumoMinActivoTotal = []
        ConsumoAvgReactivoTotal = []
        ConsumoMaxReactivoTotal = []
        ConsumoMinReactivoTotal = []
        for i in range(25):
            ConsumoAvgActivoTotal.append(np.mean(clients[:,((i*6)+2)]))
            ConsumoMaxActivoTotal.append(max(clients[:,((i*6)+3)]))
            ConsumoMinActivoTotal.append(min(clients[:,((i*6)+4)]))
            ConsumoAvgReactivoTotal.append(np.mean(clients[:,((i*6)+5)]))
            ConsumoMaxReactivoTotal.append(max(clients[:,((i*6)+6)]))
            ConsumoMinReactivoTotal.append(min(clients[:,((i*6)+7)]))
        ConsumoAvgActivoTotal = np.asarray(ConsumoAvgActivoTotal)
        ConsumoMaxActivoTotal = np.asarray(ConsumoMaxActivoTotal)
        ConsumoMinActivoTotal = np.asarray(ConsumoMinActivoTotal)
        ConsumoAvgReactivoTotal = np.asarray(ConsumoAvgReactivoTotal)
        ConsumoMaxReactivoTotal = np.asarray(ConsumoMaxReactivoTotal)
        ConsumoMinReactivoTotal = np.asarray(ConsumoMinReactivoTotal)


        fig.add_subplot(3, 2, 1)
        plt.bar(np.arange(25),ConsumoAvgActivoTotal, color='g')
        plt.xlabel('Consumo Media Activo 25 horas')
        plt.xticks(np.arange(25), np.arange(25))
        plt.grid(True)

        fig.add_subplot(3, 2, 3)
        plt.bar(np.arange(25),ConsumoMaxActivoTotal, color='b')
        plt.xlabel('Consumo Max Activo 25 horas')
        plt.xticks(np.arange(25), np.arange(25))
        plt.grid(True)

        fig.add_subplot(3, 2, 5)
        plt.bar(np.arange(25),ConsumoMinActivoTotal, color='r')
        plt.xlabel('Consumo Min Activo 25 horas')
        plt.xticks(np.arange(25), np.arange(25))
        plt.grid(True)

        fig.add_subplot(3, 2, 2)
        plt.bar(np.arange(25),ConsumoAvgReactivoTotal, color='g')
        plt.xlabel('Consumo Media Reactivo 25 horas')
        plt.xticks(np.arange(25), np.arange(25))
        plt.grid(True)

        fig.add_subplot(3, 2, 4)
        plt.bar(np.arange(25),ConsumoMaxReactivoTotal, color='b')
        plt.xlabel('Consumo Max Reactivo 25 horas')
        plt.xticks(np.arange(25), np.arange(25))
        plt.grid(True)

        fig.add_subplot(3, 2, 6)
        plt.bar(np.arange(25),ConsumoMinReactivoTotal, color='r')
        plt.xlabel('Consumo Min Activo 25 horas')
        plt.xticks(np.arange(25), np.arange(25))
        plt.grid(True)
        fig.savefig('{2}/graficas/cluster_{0}_of_{1}_consumo_stats.png'.format(c, n_clusters, sys.argv[3]))
        plt.close(fig)

        fig = plt.figure(figsize=(1080/my_dpi, 1920/my_dpi), dpi=my_dpi)

        fig.add_subplot(3, 1, 1)
        T1 = len(np.where(clients[:,0]==1.)[0])
        T2 = len(np.where(clients[:,0]==2.)[0])
        cnaes = np.array([T1, T2])
        ax = fig.gca()
        if(T1 > T2): explode = [0,0.1]
        else : explode = [0.1,0]
        ax.pie(cnaes, explode=explode, labels=['T1','T2'], colors=['lightskyblue', 'lightcoral'],autopct='%d%%', shadow=True, startangle=90)
        plt.xlabel('Cnaes')
        plt.grid(True)

        fig.add_subplot(3, 1, 2)
        M1 = len(np.where(mercados==1.)[0])
        M2 = len(np.where(mercados==2.)[0]) 
        clientesMercado = np.array([M1, M2])
        ax = fig.gca()
        if(M1 > M2): explode = [0,0.1]
        else : explode = [0.1,0]
        ax.pie(clientesMercado, explode=explode, labels=['M1','M2'], colors=['lightskyblue', 'lightcoral'],autopct='%d%%', shadow=True, startangle=90)
        plt.xlabel('Mercados')
        plt.grid(True)

        productsIDSize = np.unique(productos)
        productosTotal = len(productos)
        otros = 0
        productosTmp = []
        productosLabels = []
        colorsList = []
        explode = []
        colors = get_cmap(len(productsIDSize)+2)
        for i in range(len(productsIDSize)):
            ID = productsIDSize[i]
            productosOfID = np.where(productos==ID)[0]            
            porcentaje = ((len(productosOfID)*100)/productosTotal)
            if(porcentaje==0):
                otros = otros + len(productosOfID)
            else:
                productosLabels.append('P{0}'.format(i))
                colorsList.append(colors(i))
                productosTmp.append(len(productosOfID))
                explode.append(0)
        if otros >0:
            productosLabels.append('Otros')
            colorsList.append(colors(len(productsIDSize)+1))
            productosTmp.append(otros)
            explode.append(0)

        productosTmp = np.asarray(productosTmp)
        productoMax = max(productosTmp)
        index = np.argwhere(productosTmp==productoMax)
        explode[index[0][0]] = 0.2

        fig.add_subplot(3, 1, 3)
        ax = fig.gca()
        ax.pie(productosTmp, explode=explode, labels=productosLabels, colors=colorsList,autopct='%d%%', shadow=True, startangle=90)
        plt.xlabel('Productos')
        plt.grid(True)
        fig.savefig('{2}/graficas/cluster_{0}_of_{1}_mercado_producto_cnae_stats.png'.format(c, n_clusters, sys.argv[3]))
        plt.close(fig)


def do_kmeans(data, kmeans_init):
    datanorm = normalize(data)
    clusters = int(sys.argv[2])
    X_pca = PCA(n_components=2).fit_transform(datanorm)
    kmeans = KMeans(init=kmeans_init, n_clusters=clusters, n_init=10)
    kmeans.fit(X_pca)
    labels = kmeans.predict(np.c_[X_pca[:, 0].ravel(), X_pca[:, 1].ravel()])
    centroids = kmeans.cluster_centers_
    return showClusterGraphic(X_pca, labels, centroids)
     

def showClusterGraphic(X_pca, labels, centroids):
    clusters = int(sys.argv[2])
    colors = get_cmap(len(np.unique(labels))+2)

    my_dpi = 96
    fig = plt.figure(figsize=(2000/my_dpi, 2000/my_dpi), dpi=my_dpi)   

    x_min, x_max = X_pca[:, 0].min() - 1, X_pca[:, 0].max() + 1
    y_min, y_max = X_pca[:, 1].min() - 1, X_pca[:, 1].max() + 1
    plt.xlim(x_min, x_max)
    plt.ylim(y_min, y_max)
    plt.xticks(())
    plt.yticks(())
    for i in range(len(X_pca)):
        plt.text(X_pca[i][0], X_pca[i][1], '.', color=colors(labels[i]))

    for i in range(len(centroids)):
        plt.text(centroids[i][0], centroids[i][1], i, size=25,color='b',zorder=10)

    #plt.scatter(centroids[:, 0], centroids[:, 1],marker='x', s=50, linewidths=3,color='b', zorder=10)         
    fig.tight_layout()
    fig.savefig('{2}/graficas/kmeans_cluster_{0}_{1}_dot.png'.format(clusters, sys.argv[1][:-4], sys.argv[3]))
    plt.close(fig)

    fig = plt.figure(figsize=(2000/my_dpi, 2000/my_dpi), dpi=my_dpi)   

    x_min, x_max = X_pca[:, 0].min() - 1, X_pca[:, 0].max() + 1
    y_min, y_max = X_pca[:, 1].min() - 1, X_pca[:, 1].max() + 1
    plt.xlim(x_min, x_max)
    plt.ylim(y_min, y_max)
    plt.xticks(())
    plt.yticks(())
    for i in range(len(X_pca)):
        plt.text(X_pca[i][0], X_pca[i][1], i, color=colors(labels[i]))

    #for i in range(len(centroids)):
    #    plt.text(centroids[i][0], centroids[i][1], i, size=25,color='b',zorder=10)

    #plt.scatter(centroids[:, 0], centroids[:, 1],marker='x', s=50, linewidths=3,color='b', zorder=10)      
    fig.tight_layout()
    fig.savefig('{2}/graficas/kmeans_cluster_{0}_{1}_id.png'.format(clusters, sys.argv[1][:-4], sys.argv[3]))
    plt.close(fig)
    return labels

def to_csv(leyend, data, kmeans_labels, filename):
    leyend.append('category')
    leyend.append('indentificador')

    leyend = (','.join(leyend)).replace('\n', '')

    with open(filename, 'w') as csv:
        csv.write('%s\n' % leyend)

        for d, l, i in zip(data, kmeans_labels, range(len(data))):
            dat =["%.2f" % n for n in d] + [l] + [i+1]
            csv.write('%s\n' % (','.join(str(e) for e in dat)))

if __name__ == "__main__":
    data, producto, mercado, leyend= load_data(sys.argv[1])
    #datanorm, init = do_bench_k_means(data, producto)
    
    init = 'k-means++'
    kmeans_labels = do_kmeans(data, init)
    stats(data,kmeans_labels, producto,mercado)

    showStatsGraphic(data,kmeans_labels, producto,mercado)

    to_csv(leyend, data,kmeans_labels, '%s/result_%s_%s.csv' % (sys.argv[3], sys.argv[1][:-4], sys.argv[2]))
