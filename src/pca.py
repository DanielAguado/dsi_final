# -*- coding: utf-8 -*-

import gc
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from scipy import cluster
from sklearn import preprocessing
from sklearn.decomposition import PCA
from sklearn import metrics
import sklearn.neighbors
import numpy
import sys


def normalize(d):
    min_max_scaler = preprocessing.MinMaxScaler()
    datanorm = min_max_scaler.fit_transform(d)
    return datanorm

def pca(d):
    estimator = PCA (n_components = 2)
    X_pca = estimator.fit_transform(d)
    return X_pca
    
def load_data(database):
    f = open(database, "r")
    data = []
    for l in f.readlines():
        row = l.split(",")
        del row[0:4]
        try:
            data.append(map(float,row))
        except ValueError:
            print row
    return data

def show_pca():
    data = load_data(sys.argv[1])
    datanorm = normalize(data)
    print "normalize done"
    X_pca = pca(datanorm)
    print "pca done"
    my_dpi = 96
    fig = plt.figure(figsize=(2000/my_dpi, 2000/my_dpi), dpi=my_dpi)        
    plt.plot(X_pca[:,0], X_pca[:,1],'x')
    plt.show()
    fig.savefig('graficas/pca_'+sys.argv[1]+'.png')
    plt.close(fig)
    return datanorm


def dendogram(d,method):
    dist = sklearn.neighbors.DistanceMetric.get_metric('euclidean')
    matsim = dist.pairwise(d)
    avSim = numpy.average(matsim)
    print "%s\t%6.2f" % ('Distancia Media', avSim)

    # 3.2. Building the Dendrogram    
    fig = plt.figure(figsize=(2000/my_dpi, 2000/my_dpi), dpi=my_dpi)        
    clusters = cluster.hierarchy.linkage(matsim, method = method)
    # http://docs.scipy.org/doc/scipy-0.14.0/reference/generated/scipy.cluster.hierarchy.dendrogram.html
    cluster.hierarchy.dendrogram(clusters, color_threshold=0)
    fig.savefig('graficas/dendograma_'+sys.argv[1]+'.png')
    plt.close(fig)
    return clusters



if __name__ == "__main__":

    datanorm = show_pca()
    #clusters = dendogram(datanorm, 'complete')

