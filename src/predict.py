#!/usr/bin/python3

import sqlite3
import os
import sys
import datetime
import random
import math

def get_testing_dataset(filename):
    dataset = []
    first_line = True
    with open(filename, 'r') as f:
        for line in f:
            data = line.replace('\n', '').split(',')
            if first_line == True:
                first_line = False
            else:
                result = [i for i in data]
                del result[-1] #remove identificador
                dataset.append(result)

    return dataset

def get_dataset_clusterized(filename):
    dataset = dict()
    first_line = True

    with open(filename, 'r') as f:
        for line in f:
            data = line.replace('\n', '').split(',')
            if first_line == True:
                first_line = False
            else:
                key = data[-2]

                if key not in dataset:
                    dataset[key] = []
                result = [float(i) for i in data]
                del result[-1]
                dataset[key].append(result)

    return dataset

def mean(numbers):
    return sum(numbers)/float(len(numbers))

def stdev(numbers):
    avg = mean(numbers)
    variance = sum([pow(x-avg,2) for x in numbers])/float(len(numbers)-1)
    return math.sqrt(variance)

def summarize(dataset):
    summaries = [(mean(attribute), stdev(attribute)) for attribute in zip(*dataset)]
    del summaries[-1]

    return summaries

def summarizeByClass(data):
    summaries = dict()
    for classValue, instances in data.items():
        summaries[classValue] = summarize(instances)
    return summaries

def calculateProbability(x, mean, stdev):
    try:
        exponent = math.exp(-(math.pow(x-mean,2)/(2*math.pow(stdev,2))))
        return (1 / (math.sqrt(2*math.pi) * stdev)) * exponent
    except:
        #print ("x: %s\tmean: %s\tstdev: %s" % (x, mean, stdev))
        return 1

def calculateClassProbabilities(summaries, inputVector):
    probabilities = dict()
    for classValue, classSummaries in summaries.items():
        probabilities[classValue] = 1
        for i in range(len(classSummaries)):
            mean, stdev = classSummaries[i]
            x = inputVector[i]
            probabilities[classValue] *= calculateProbability(x, mean, stdev)
    return probabilities

def get_membership_percent(probabilities):
    print ("prob: %s" % probabilities)
    membership_percent = dict()
    aux = []

    for classValue, probability in probabilities.items():
        if probability == 0:
            probability = 1
        aux.append((classValue, probability))

    for i in range(len(aux)):
        membership_percent[aux[i][0]] =  100 * aux[i][1] / (sum([x[1]  for x in aux]))

    return membership_percent


def predict(summaries, inputVector):
    probabilities = calculateClassProbabilities(summaries, inputVector)
    bestLabel, bestProb = None, -1
    for classValue, probability in probabilities.items():
        if bestLabel is None or probability > bestProb:
            bestProb = probability
            bestLabel = classValue

    return bestLabel, get_membership_percent(probabilities)

def getPredictions(summaries, testSet):
    predictions = []
    membership_percent_group = []
    for i in range(len(testSet)):
        result, membership_percent = predict(summaries, testSet[i])
        predictions.append(result)
        membership_percent_group.append(membership_percent)
    return predictions, membership_percent_group
 
def getAccuracy(testSet, predictions):
    correct = 0
    for x in range(len(testSet)):
        if testSet[x][-1] == predictions[x]:
            correct += 1
    return (correct/float(len(testSet))) * 100.0


if __name__ == '__main__':
    clustered_dataset = sys.argv[1]
    testing_dataset = sys.argv[2]

    dataset_clusterized = get_dataset_clusterized(clustered_dataset)

    test_data = get_testing_dataset(testing_dataset)

    summaries = summarizeByClass(dataset_clusterized)
    #print (summaries)
    predictions, membership_percent_group = getPredictions(summaries, test_data)
    accuracy = getAccuracy(test_data, predictions)
    #print (accuracy)
    print('Accuracy: {0}%').format(accuracy)
    print('membership percents: {0}%').format(membership_percent_group)
