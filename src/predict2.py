#!/usr/bin/python3

from sklearn.naive_bayes import GaussianNB
import numpy as np
import sys

def get_testing_dataset(filename):
    dataset = []
    categories = []
    first_line = True
    with open(filename, 'r') as f:
        for line in f:
            data = line.replace('\n', '').split(',')
            if first_line == True:
                first_line = False
            else:
                result = [float(i) for i in data]
                del result[-1] # identificador
                del result[-1] #category
                categories.append(data[-2])    
                dataset.append(result)

    return dataset, categories

def get_dataset_clusterized(filename):
    dataset = []
    categories = []
    first_line = True

    with open(filename, 'r') as f:
        for line in f:
            data = line.replace('\n', '').split(',')
            if first_line == True:
                first_line = False
            else:
                result = [float(i) for i in data]                
                del result[-1] # identificador
                del result[-1] #category
                categories.append(data[-2])
                dataset.append(result)

    return dataset, categories


if __name__ == '__main__':
    clusted_dataset = sys.argv[1]
    testing_dataset = sys.argv[2]

    dataset, categories = get_dataset_clusterized(clusted_dataset)

    test_data, categories_tests = get_testing_dataset(testing_dataset)


    model = GaussianNB()

    #print "Dataset: \n%s" % dataset[0]
    #print "Testdata: \n%s" % test_data[0]

    # Train the model using the training sets 

    model.fit(dataset, categories)



    #Predict Output 

#    predicted = model.predict_log_proba(test_data)

    #print predicted

    print "groups"
    predicted = model.predict(test_data)

    print predicted   

    accuracy = model.score(test_data, categories_tests)

    print accuracy
