# -*- coding: utf-8 -*-


import matplotlib
import numpy as np
matplotlib.use('Agg')
import matplotlib.pyplot as plt

def load_data():
    f = open("dataset_analysis_producto.txt", "r")
    data = []
    for l in f.readlines():
        row = l.split(",")
        row[0] = row[0][1:]
        try:
            data.append(map(float,row))
        except ValueError:
            print row
    return data

def stat(data):
    my_dpi = 96
    for d in data:
        d_tmp = np.array(d)
        fig = plt.figure(figsize=(2000/my_dpi, 2000/my_dpi), dpi=my_dpi)        
        avg_consumo = d_tmp[np.arange(1,len(d_tmp),3)]
        max_consumo = d_tmp[np.arange(2,len(d_tmp),3)]
        min_consumo = d_tmp[np.arange(3,len(d_tmp),3)]
        print "P{0}:avg{1}\nmax{2}\nmin{3}\n".format(d_tmp[0],avg_consumo, max_consumo, min_consumo)
        plt.bar(np.arange(25),max_consumo, color='b')
        plt.bar(np.arange(25),avg_consumo, color='g')
        plt.bar(np.arange(25),min_consumo, color='r')
        plt.xlabel('Poductos')
        plt.xticks(np.arange(25), np.arange(25))
        fig.savefig('graficas_producto/test_P{0}.png'.format(format(d[0])))
        plt.close(fig)

data = load_data()
stat = stat(data)
